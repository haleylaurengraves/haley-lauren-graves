Haley Lauren Graves is an outgoing, ambitious, Sport and Exercise Science major from Florida. Haley has an extensive background in animal care and currently serves as a technician at a local animal hospital. In her spare time, Haley serves her community by working with multiple animal rescues, assisting with facility upkeep and adoptions.

Website: http://haleylaurengraves.com
